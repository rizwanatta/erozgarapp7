import React from "react";
import { BaseNavigator } from "./src/navigations/baseNavigator";
import { useFonts } from 'expo-font';

export default function APP() {


const [fontsLoaded] = useFonts({
    'heavy': require('./assets/fonts/Gilroy-Heavy.ttf'),
    'regular': require('./assets/fonts/Gilroy-Regular.ttf'),
  });

  return <BaseNavigator />;
}
