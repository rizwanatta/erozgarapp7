import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { Ionicons } from "@expo/vector-icons";

const Header = ({ title, onBackPress }) => {
  return (
    <View style={styles.header}>
      <Ionicons name="chevron-back" size={33} onPress={onBackPress} />
      <Text style={styles.headerText}>{title}</Text>
    </View>
  );
};

export { Header };

const styles = StyleSheet.create({
  header: {
    flex: 0.5,
    justifyContent: "space-between",
    paddingHorizontal: 10,
    paddingVertical: 20,
    marginTop: 10,
  },
  headerText: {
    fontSize: 40,
    fontFamily:'regular'
  },
});
