import { StyleSheet, View } from "react-native";
import React from "react";
import { Skeleton } from "@rneui/themed";
import { LinearGradient } from "expo-linear-gradient";

const Loader = () => {
  return (
    <View style={styles.con}>
      <Skeleton
        animation="wave"
        LinearGradientComponent={LinearGradient}
        circle
        width={80}
        height={80}
      />
      <View>
        <Skeleton
          LinearGradientComponent={LinearGradient}
          style={{ margin: 5 }}
          animation="wave"
          width={200}
          height={40}
        />
        <Skeleton
          LinearGradientComponent={LinearGradient}
          style={{ margin: 5 }}
          animation="wave"
          width={200}
          height={40}
        />
      </View>
    </View>
  );
};

export { Loader };

const styles = StyleSheet.create({
  con: {
    margin: 10,
    flexDirection: "row",
    alignItems: "center",
  },
});
