import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Login from "../screens/login";
import Register from "../screens/register";
import ForgotPassword from "../screens/forgotPassword";
import Home from "../screens/home";
import Dummy from "../screens/dummy";
import MyLocation from "../screens/mylocation";
import UserFromApi from "../screens/userFromApi";
import Settings from "../screens/settings";

const BaseStack = createNativeStackNavigator();

function BaseNavigator() {
  return (
    <NavigationContainer>
      <BaseStack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <BaseStack.Screen name={"Settings"} component={Settings} />
        <BaseStack.Screen name={"UserFromApi"} component={UserFromApi} />
        <BaseStack.Screen name={"MyLocation"} component={MyLocation} />
        <BaseStack.Screen name={"Login"} component={Login} />
        <BaseStack.Screen name={"Dummy"} component={Dummy} />
        <BaseStack.Screen name={"Register"} component={Register} />
        <BaseStack.Screen name={"ForgotPassword"} component={ForgotPassword} />
        <BaseStack.Screen name={"Home"} component={Home} />
      </BaseStack.Navigator>
    </NavigationContainer>
  );
}

export { BaseNavigator };
