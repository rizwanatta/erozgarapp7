import { StyleSheet, FlatList, View, ImageBackground } from "react-native";
import React, { useState } from "react";
import axios from "axios";
import { Avatar, Text } from "@rneui/themed";
import { LinearGradient } from "expo-linear-gradient";

import { Loader } from "../components/loader";

const UserFromApi = () => {
  const BASE_URL = "http://api.github.com/users";

  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();

  axios
    .get(BASE_URL)
    .then((response) => {
      // data aya h k nei
      if (response.data) {
        setData(response.data);
      }

      setIsLoading(false);
    })
    .catch((err) => {
      console.log(err);
      setIsLoading(false);
    });

  const __renderItem = ({ item }) => (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        margin: 10,
        padding: 10,
      }}
    >
      <Avatar size={"large"} rounded source={{ uri: item.avatar_url }} />
      <Text>{item.login}</Text>
    </View>
  );

  return (
    <ImageBackground
      source={require("../../assets/bg.png")}
      resizeMode="contain"
      style={styles.container}
    >
      {isLoading === false ? (
        <>
          <Loader />
          <Loader />
          <Loader />
          <Loader />
          <Loader />
          <Loader />
          <Loader />
          <Loader />
        </>
      ) : (
        <FlatList data={data} renderItem={__renderItem} />
      )}
    </ImageBackground>
  );
};

export default UserFromApi;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loaderCon: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
