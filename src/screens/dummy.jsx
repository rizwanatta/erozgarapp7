import {  StyleSheet } from 'react-native'
import React from 'react'
import { WebView } from 'react-native-webview';

const Dummy = () => {
  return (
    <WebView
      style={styles.container}
      source={{ uri: 'https://sanadcare.com' }}
    />  
  )
}

export default Dummy

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
});
