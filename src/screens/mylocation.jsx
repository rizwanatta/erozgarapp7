import { StyleSheet, Text, View } from "react-native";
import React from "react";
import MapView, {Marker} from "react-native-maps";

const MyLocation = () => {
  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        mapType="hybrid"
        showsUserLocation
        zoomEnabled
        zoomControlEnabled
      >

      <Marker

      title="hi hellow how are you"
       coordinate={{latitude:31.4754161,longitude: 74.3412872}}

    />

    </MapView>
    </View>
  );
};

export default MyLocation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: "100%",
    height: "100%",
  },
});
