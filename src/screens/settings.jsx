import { StyleSheet, Text, View } from "react-native";
import React, { useState } from "react";
import { Button, Avatar } from "@rneui/themed";
import * as ImagePicker from "expo-image-picker";
import { Camera, CameraType } from "expo-camera";

export default function Settings() {
  const [dp, setDp] = useState("");

  function attemptForMedia() {
    ImagePicker.launchImageLibraryAsync({
      mediaTypes: "Images",
      allowsEditing: true,
    })
      .then((response) => {
        if (response.canceled === false) {
          // means gallery is closed after selecting a picture
          setDp(response.assets[0].uri);
        }
      })
      .catch((error) => {
        console.log({ error });
      });
  }

  function attemptForCamera() {
    ImagePicker.requestCameraPermissionsAsync();
    ImagePicker.launchCameraAsync({ mediaTypes: "Images", allowsEditing: true })
      .then((response) => {
        if (response.canceled === false) {
          //user has taken a photo
          setDp(response.assets[0].uri);
        }
      })
      .catch((error) => {
        console.log({ error });
      });
  }

  return (
    <View style={styles.con}>
      <Avatar source={{ uri: dp }} size={"xlarge"} rounded />
      <Button type="solid" title={"Gallery Picker"} onPress={attemptForMedia} />
      <Button type="solid" title={"Camera"} onPress={attemptForCamera} />

      <Camera style={styles.camera} type={CameraType.front}></Camera>
    </View>
  );
}

const styles = StyleSheet.create({
  con: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  camera: {
    height: 400,
    width: "100%",
  },
});
