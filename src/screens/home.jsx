import { StyleSheet, View, FlatList } from "react-native";
import React from "react";

import postsData from "../database/data.json";
import { Avatar, Text, Icon, Image } from "@rneui/themed";

const Home = () => {
  const __renderItem = ({ item }) => (
    <View style={styles.itemCon}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginRight: 10,
          justifyContent: "space-between",
          paddingHorizontal: 10,
        }}
      >
        <View
          style={{
            paddingVertical: 10,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Avatar size={"medium"} rounded source={{ uri: item.thumbnail }} />
          <Text style={{ 
            fontFamily:'heavy',
            fontSize:20,
            color: "white", marginHorizontal: 10 }}>
            {item.username}
          </Text>
          <Icon name={"verified"} type={"material"} color={"blue"} />
        </View>

        <Icon name={"options"} type={"simple-line-icon"} color={"white"} />
      </View>

      <Image
        style={{ height: 300, width: "100%" }}
        source={{ uri: item.picture }}
      />

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          padding: 10,
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Icon
            name={"heart"}
            type={"font-awesome"}
            color={"white"}
            style={{ marginHorizontal: 10 }}
          />
          <Icon
            name={"comment"}
            type={"font-awesome"}
            color={"white"}
            style={{ marginHorizontal: 10 }}
          />
          <Icon
            name={"share"}
            type={"font-awesome"}
            color={"white"}
            style={{ marginHorizontal: 10 }}
          />
        </View>

        <Icon
          name={"bookmark"}
          type={"font-awesome"}
          color={"white"}
          style={{ marginRight: 10 }}
        />
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <FlatList data={postsData} renderItem={__renderItem} />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },

  itemCon: {
    width: "100%",
    marginHorizontal: 10,
    backgroundColor: "black",
    marginVertical: 10,
  },
});
