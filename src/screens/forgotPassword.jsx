import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Text,
  View,
} from "react-native";
import React, { useRef } from "react";
import { Header } from "../components/header";
import ConfettiCannon from "react-native-confetti-cannon";

const ForgotPassword = () => {
  const confettiRef = useRef();

  function submitPress() {
    if (confettiRef) {
      confettiRef.current.start();
    }
  }

  return (
    <View style={styles.con}>
      <View style={styles.headerCon}>
        <Header title={"Forgot Password"} />
      </View>

      <View style={styles.form}>
        <TextInput style={styles.input} placeholder="email" />
        <TouchableOpacity style={styles.button} onPress={submitPress}>
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </View>
      <ConfettiCannon ref={confettiRef} count={200} origin={{ x: -10, y: 0 }} />
    </View>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  con: {
    flex: 1,
    backgroundColor: "rgba(255,255,244,0.8)",
  },
  headerCon: {
    height: 300,
    width: "100%",
  },
  form: {
    paddingHorizontal: 20,
  },
  input: {
    backgroundColor: "white",
    padding: 15,
    marginVertical: 10,
  },
  button: {
    width: "90%",
    padding: 10,
    backgroundColor: "red",
    borderRadius: 20,
    alignItems: "center",
    alignSelf: "center",
  },
  buttonText: {
    color: "white",
  },
});
